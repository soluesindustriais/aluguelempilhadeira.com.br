<?
$h1         = 'Mapa do site';
$title      = 'Mapa do site';
$desc       = 'O mapa do site com todos os atalhos para todas as páginas deste site. Qualquer dúvida estamos a disposição por email ou telefone. Clicando aqui';
$key        = 'atalho para as páginas do site, mapa do site';
$var        = 'Mapa do site';

include('inc/head.php');
?>
</head>
<body>

<? include('inc/topo.php');?>
<div class="wrapper">
    
    <main>
    <div class="content">
        <section>
            <?=$caminho?>
            <h1><?=$h1?></h1>
            <ul class="sub-menu">
                <? include('inc/mapa-menu-top.php');?>
                <a class="categoria-sitemap" href="<?=$url?>produtos" title="Produtos">Produtos</a>
               <a href="<?=$url?>aluguel-de-empilhadeira-e-acessorios-para-docas-categoria" title="Aluguel de Empilhadeira">Aluguel de Empilhadeira</a>
                <li><? include('inc/aluguel-de-empilhadeira-e-acessorios-para-docas/aluguel-de-empilhadeira-e-acessorios-para-docas-sub-menu.php');?></li>
                <a class="categoria-sitemap" href="<?=$url?>empilhadeiras-categoria" title="Empilhadeiras">Empilhadeiras</a>
                <li><? include('inc/empilhadeiras/empilhadeiras-sub-menu.php');?></li>
                <a class="categoria-sitemap" href="<?=$url?>carrinho-rebocador-categoria" title="Carrinho Rebocador">Carrinho Rebocador</a>
                <li><? include('inc/carrinho-rebocador/carrinho-rebocador-sub-menu.php');?></li>
            </ul>
            
            <br class="clear">
            
        </section>
    </div>
    </main>
    </div><!-- .wrapper -->
    <? include('inc/footer.php');?>
</body>
</html>