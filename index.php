<?
$h1         = 'Aluguel Empilhadeira';
$title      = 'Início';
$desc       = 'Aluguel Empilhadeira | Cote empilhadeiras de diversos modelos com inúmeras empresas em um só lugar, no maior portal de empilhadeiras. Tudo gratuitamente';  
$var        = 'Home';
include('inc/head.php');

?>
</head>
<body>
<? include('inc/topo.php'); ?>
<section class="cd-hero">
  <div class="title-main"> <h1>EMPILHADEIRA</h1> </div>
  <ul class="cd-hero-slider autoplay">
    <li class="selected">
      <div class="cd-full-width">
        <h2>EMPILHADEIRA ELÉTRICA</h2>
        <p>O uso da empilhadeira elétrica permite que o colaborador que conduz a máquina se movimente por ambientes estreitos por causa de sua praticidade. Com facilidade de manobrar, a empilhadeira elétrica é também bastante econômica por conta do seu custo benefício.</p>
        <a href="<?=$url?>empilhadeira-eletrica" class="cd-btn">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>EMPILHADEIRA HYSTER</h2>
        <p>A Empilhadeira Hyster é um dos produtos de mais qualidade no mercado de empilhadeiras e estas e outras marcas de empilhadeiras estão disponíveis para comercialização e locação.</p>
        <a href="<?=$url?>empilhadeira-hyster" class="cd-btn">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>LOCAÇÃO DE EMPILHADEIRA</h2>
        <p>A Locação de empilhadeira é uma modalidade de negócios considerada madura no Brasil, devido aos benefícios já contabilizados pelas empresas. As empilhadeiras são utilizadas, principalmente, para carregar e descarregar mercadorias em paletes e exige cuidados específicos para manutenção e fornecimento de peças.</p>
        <a href="<?=$url?>locacao-de-empilhadeira" class="cd-btn">Saiba mais</a>
      </div>
    </li>
  </ul>
  <div class="cd-slider-nav">
    <nav>
      <span class="cd-marker item-1"></span>
      <ul>
        <li class="selected"><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
      </ul>
    </nav>
  </div>
</section>
<main>
  <section class="wrapper-main">
    <div class="main-center">
      <div class=" quadro-2 ">
        <h2>EMPILHADEIRAS</h2>
        <div class="div-img">
          <p data-anime="left-0">Toda empresa necessita de um profissional responsável pela operação de empilhadeiras para prevenir possíveis interrupções e imprevistos. Todas as empresas têm como uma de suas necessidades básicas o transporte e o içamento de cargas. Por isso, o investimento em treinamento especializado é fator determinante para evitar prejuízos e graves acidentes. A movimentação de máquinas e o içamento de cargas não permite erros.</p>
        </div>
        <div class="gerador-svg" data-anime="in">
          <img src="imagens/img-home/aluguel-empilhadeira.jpg" alt="Aluguel Empilhadeira" title="Aluguel Empilhadeira">
        </div>
      </div>
      <div class=" incomplete-box">
        <ul data-anime="in">
          <li>
            <p>No caso das empilhadeiras, existem diversos tipos e modelos no mercado, além dos diferentes tipos de serviço. Segue abaixo alguns exemplos que oferecemos:</p>
            <li><i class="fas fa-angle-right"></i> <a class="linkagem" href="<?=$url?>empilhadeira-eletrica">Empilhadeira elétrica</a></li>
            <li><i class="fas fa-angle-right"></i> <a class="linkagem" href="<?=$url?>empilhadeira-manual">Empilhadeira manual</a></li>
            <li><i class="fas fa-angle-right"></i> <a class="linkagem" href="<?=$url?>aluguel-de-empilhadeira">Aluguel de empilhadeira</a></li>
            <li><i class="fas fa-angle-right"></i> <a class="linkagem" href="<?=$url?>locacao-de-empilhadeira">Locação de empilhadeira</a></li>
            <li><i class="fas fa-angle-right"></i> <a class="linkagem" href="<?=$url?>empilhadeira-retratil">Empilhadeira retrátil</a></li>
          </ul>
          <a href="<?=$url?>empilhadeira-aluguel" class="btn-4">Saiba mais</a>
        </div>
      </div>
      <div id="content-icons">
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <img src="imagens/boxes.png" class="fas fa-truck-loading fa-7x" alt="Box"/>
            <div>
              <p>OPTIMIZE SEU CARREGAMENTO DE CARGA</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <img src="imagens/shield.png" class="fas fa-shield-alt fa-7x" alt="Escudo"/>
            <div>
              <p>SEGURANÇA PARA LOCAÇÃO</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <img src="imagens/enterprise.png" class="far fa-building fa-7x" alt="Empresa"/>
            <div>
              <p>COTE COM DIVERSAS EMPRESAS</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <img src="imagens/money-bag.png" class="fas fa-dollar-sign fa-7x" alt="Dinheiro"/>
            <div>
              <p>COMPARE PREÇOS</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <img src="imagens/clock.png" class="far fa-clock" alt="Relogio"/>
            <div>
              <p>ECONOMIZE TEMPO</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <img src="imagens/handshake.png" class="far fa-handshake fa-7x" alt="Handshake"/>
            <div>
              <p>FAÇA O MELHOR NEGÓCIO</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="wrapper-img">
      <div class="txtcenter">
        <h2>Produtos <b>Relacionados</b></h2>
      </div>
       <div class="content-icons">

        <div class="produtos-relacionados-1">
          <figure>
            <div class="fig-img2">
            <a href="<?=$url?>empilhadeira-eletrica">
              <h2>EMPILHADEIRA ELÉTRICA</h2>
                </a>
                <a href="<?=$url?>empilhadeira-eletrica">
                <span class="btn-5" data-anime="up"> Saiba Mais </span>
              </a>
              </div>
          </figure>
        </div>

        <div class="produtos-relacionados-2">
          <figure class="figure2">
              <div class="fig-img2">
            <a href="<?=$url?>empilhadeira-a-gas">
                <h2>EMPILHADEIRA A GÁS</h2>
            </a>
                <a href="<?=$url?>empilhadeira-a-gas">
                <span class="btn-5" data-anime="up"> Saiba Mais </span>
                </a>
              </div>
          </figure>
        </div>

        <div class="produtos-relacionados-3">
          <figure>
              <div class="fig-img2">
              <a href="<?=$url?>empilhadeira-manual">
                <h2>EMPILHADEIRA MANUAL</h2>
                </a>
                <a href="<?=$url?>empilhadeira-manual">
                <span class="btn-5" data-anime="up"> Saiba Mais </span>
                </a>
              </div>
          </figure>
        </div>
      </div>
    </section>
    <section class="wrapper-destaque">
      <div class="destaque txtcenter">
        <h2>Galeria de <b>Produtos</b></h2>
        <div class="center-block txtcenter">
          <ul class="gallery">
            <li><a href="<?=$url?>imagens/img-home/galeria-empilhadeira-a-gas.jpg" class="lightbox" title="Empilhadeira a gás">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-empilhadeira-a-gas.jpg" title="Empilhadeira a gás" alt="Empilhadeira a gás">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-empilhadeira-diesel.jpg" class="lightbox"  title="Empilhadeira diesel">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-empilhadeira-diesel.jpg" alt="Empilhadeira diesel" title="Empilhadeira diesel">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-empilhadeira-manual-eletrica.jpg" class="lightbox" title="Empilhadeira manual elétrica">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-empilhadeira-manual-eletrica.jpg" alt="Empilhadeira manual elétrica" title="Empilhadeira manual elétrica">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-empilhadeira-a-gas-nova-preco.jpg" class="lightbox" title="Empilhadeira a gás nova preço">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-empilhadeira-a-gas-nova-preco.jpg" alt="Empilhadeira a gás nova preço" title="Empilhadeira a gás nova preço">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-empilhadeira-trilateral.jpg" class="lightbox" title="Empilhadeira trilateral">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-empilhadeira-trilateral.jpg" alt="Empilhadeira trilateral"  title="Empilhadeira trilateral">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-locacao-de-empilhadeira.jpg" class="lightbox" title="Locação de empilhadeira">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-locacao-de-empilhadeira.jpg" alt="Locação de empilhadeira" title="Locação de empilhadeira">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-empilhadeira-trilateral-preco.jpg" class="lightbox" title="Empilhadeira trilateral preço">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-empilhadeira-trilateral-preco.jpg" alt="Empilhadeira trilateral preço" title="Empilhadeira trilateral preço">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-empilhadeira-eletrica-contrabalancada.jpg" class="lightbox" title="Empilhadeira elétrica contrabalancada">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-empilhadeira-eletrica-contrabalancada.jpg" alt="Empilhadeira elétrica contrabalancada" title="Empilhadeira elétrica contrabalancada">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-preco-de-empilhadeira.jpg" class="lightbox" title="Preço- de empilhadeira">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-preco-de-empilhadeira.jpg" alt="Preço- de empilhadeira" title="Preço- de empilhadeira">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-empilhadeira-eletrica.jpg" class="lightbox" title="Empilhadeira elétrica">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-empilhadeira-eletrica.jpg" alt="Empilhadeira elétrica" title="Empilhadeira elétrica">
              </a>
              </li>
          </ul>
        </div>
      </div>
    </section>
</main>
<? include('inc/footer.php'); ?>
<script src="<?=$url?>hero/js/modernizr.js"></script>
<script src="<?=$url?>hero/js/main.min.js"></script>
</body>
</html>