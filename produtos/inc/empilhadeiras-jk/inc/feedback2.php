<section class="feedback">
    <div class="wrapper d-flex justify-content-around flex-wrap-mobile">
    <div style="width: 50%;">
        <h2 style="font-size: 2.5rem;">About us</h2>
        <p style="max-width: none; text-align: left; margin-bottom: 20px">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quidem, ut magni reprehenderit, magnam suscipit totam rem labore odit maiores aperiam sequi impedit officia laboriosam dolores deserunt. Impedit odio totam deserunt.</p>
        <p style="max-width: none; text-align: left; margin-bottom: 50px">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quidem, ut magni reprehenderit, magnam suscipit totam rem labore odit maiores aperiam sequi impedit officia laboriosam dolores deserunt. Impedit odio totam deserunt.</p>
        <a href="<?= $url ?>duda-godoi" title="duda godoi" style="background-color: var(--color-secundary); color: #fff; padding: 10px 60px;">CTA</a>

    </div>
    <div style="padding: 50px 30px; background-color:var(--light); border-radius: 12px; box-shadow: 0px 0px 10px 3  px rgba(0,0,0,0.29) inset;">
        <div class="swiper wrapper" style="width: 400px;">
            <div class="swiper-wrapper">
                <!-- Slides -->
                <div class="swiper-slide">
                    <div class="slide-feedback">
                        <i class="fa-regular fa-comments"></i>
                        <h2>"Sailor Moon é um clássico atemporal, repleto de ação mágica e amizade."</h2>
                        <img src="imagens/image-padrao.jpg" alt="imagem feedback">
                        <span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                        <div class="user">
                            <h3>Dudinha Godoi</h3>
                            <h3>Brazil</h3>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="slide-feedback">
                        <i class="fa-regular fa-comments"></i>
                        <h2>"Sailor Moon é um clássico atemporal, repleto de ação mágica e amizade."</h2>
                        <img src="imagens/image-padrao.jpg" alt="imagem feedback">
                        <span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                        <div class="user">
                            <h3>Dudinha Godoi</h3>
                            <h3>Brazil</h3>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="slide-feedback">
                        <i class="fa-regular fa-comments"></i>
                        <h2>"Sailor Moon é um clássico atemporal, repleto de ação mágica e amizade."</h2>
                        <img src="imagens/image-padrao.jpg" alt="imagem feedback">
                        <span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                        <div class="user">
                            <h3>Dudinha Godoi</h3>
                            <h3>Brazil</h3>
                        </div>
                    </div>
                </div>

            </div>

            <!-- If we need navigation buttons -->
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </div>
    </div>
</section>

<script>
    const swiper = new Swiper('.swiper', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,


        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },

        // And if we need scrollbar
        scrollbar: {
            el: '.swiper-scrollbar',
        },
    });
</script>