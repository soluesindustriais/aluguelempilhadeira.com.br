<section class="anuncio-paginas-relacionadas wrapper">
<? $linkminisiteprodpop = ($linkminisitenb == "") ? "." : "$linkminisitenb" ?>
    <article class="article-paginas-relacionadas">
        <h2 class="h2-paginas-relacionadas">Produtos <span class="color-secundary-mpi">Relacionados</span></h2>
    </article>
    <div class="paginas-relacionadas-card flex-wrap-mobile light-box-shadow">
        <?php
        $random = array();
        $limit = 4;
        foreach ($VetPalavrasProdutos  as $pagina) {
            $palavraSemAcento = strtolower(remove_acentos($pagina));
            $palavraComHifen = ucwords(str_replace(" ", "-", $pagina));
            $palavraSemHifenUpperCase = ucwords(str_replace("-", " ", $pagina));
            $random[] = "<div class='card-pagina-relacionada light-box-shadow'>
                    <img src='$linkminisiteprodpop/imagens/informacoes/$palavraSemAcento-1.webp' alt='$palavraSemHifenUpperCase'>
                    <p class='p-pagina-relacionada'>$palavraSemHifenUpperCase</p>
                    <a href=\"" . $url . $linkminisite . $palavraSemAcento . "\" title=\"$palavraSemHifenUpperCase\">Acesse a página</a></li>
                    </div>  
                    ";
        }

        shuffle($random);
        for ($i = 0; $i < $limit; $i++) {
            echo $random[$i];
        }
        ?>
        <?php
        $random = array();
        $limit = 4;
        foreach ($VetPalavrasInformacoes  as $pagina) {
            $palavraSemAcento = strtolower(remove_acentos($pagina));
            $palavraComHifen = ucwords(str_replace(" ", "-", $pagina));
            $palavraSemHifenUpperCase = ucwords(str_replace("-", " ", $pagina));
            echo "<div class='card-pagina-relacionada light-box-shadow'>
                    <img src='$linkminisiteprodpop/imagens/informacoes/$palavraSemAcento-1.webp' alt='$palavraSemHifenUpperCase'>
                    <p class='p-pagina-relacionada'>$palavraSemHifenUpperCase</p>
                    <a href=\"" . $url . $linkminisite . $palavraSemAcento . "\" title=\"$palavraSemHifenUpperCase\">Acesse a página</a></li>
                    </div>  
                    ";
        }
?>
        

</section>