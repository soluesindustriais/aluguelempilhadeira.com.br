<?php foreach ($menu as $key => $value): ?>
    <? if($sigMenuPosition !== false && $key == $sigMenuPosition) include "$linkminisite"."inc/menu-top-inc.php"; ?>
    <li <?=($value[2] ? ' class="dropdown"' : '')?> <?=($value[3] ? ' data-icon-menu' : '')?>>
        <a href="<?=(strpos($value[0], 'http') !== false ? $value[0] : $url.$value[0])?>" title="<?=($value[1] == 'Home' ? 'Página inicial' : $value[1])?>" <?=(strpos($value[0], 'http') !== false || strpos($value[0], '.pdf') !== false ? 'target="_blank" rel="nofollow"' : "")?>>
            <?php if(!$isMobile && $value[3]): 
                    if($value[1] !== 'informacoes'): ?>
                        <i class="<?=$value[3]?>"></i>
                        <span class="d-block"><?=$value[1]?></span>
                    <?php else: ?>
                        <i class="<?=$value[3]?> fa-xl"></i>
                    <?php endif; ?>
            <?php else: ?>
                <?=$value[1];?>
            <?php endif; ?>
        </a>
        <?php if($value[2]): echo $value[0] == 'informacoes' ? '<ul class="sub-menu">' : '<ul class="sub-menu">';
            include("$linkminisite".'inc/'.$value[2].'.php'); 
            ?>
        </ul>
    <?php endif; ?>
</li>
<?php endforeach; ?>


<!-- QUANDO O MINI SITE ESTIVER DENTRO DO SATELITE -->
<!-- <?php foreach ($menu as $key => $value): ?>
    <? if($sigMenuPosition !== false && $key == $sigMenuPosition) include "$linkminisite"."inc/menu-top-inc.php"; ?>
    <li <?=($value[2] ? ' class="dropdown"' : '')?> <?=($value[3] ? ' data-icon-menu' : '')?>>
        <a href="<?=(strpos($value[0], 'http') !== false ? $value[0] : $linksubdominio.$value[0])?>" title="<?=($value[1] == 'Home' ? 'Página inicial' : $value[1])?>" <?=(strpos($value[0], 'http') !== false || strpos($value[0], '.pdf') !== false ? 'target="_blank" rel="nofollow"' : "")?>>
            <?php if(!$isMobile && $value[3]): 
                    if($value[1] !== 'informacoes'): ?>
                        <i class="<?=$value[3]?>"></i>
                        <span class="d-block"><?=$value[1]?></span>
                    <?php else: ?>
                        <i class="<?=$value[3]?> fa-xl"></i>
                    <?php endif; ?>
            <?php else: ?>
                <?=$value[1];?>
            <?php endif; ?>
        </a>
        <?php if($value[2]): echo $value[0] == 'informacoes' ? '<ul class="sub-menu">' : '<ul class="sub-menu">';
            include("$linkminisite".'inc/'.$value[2].'.php'); 
            ?>
        </ul>
    <?php endif; ?>
</li>
<?php endforeach; ?> -->