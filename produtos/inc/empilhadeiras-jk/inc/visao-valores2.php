<section class="flex-wrap-mobile section-content" style="background-color:var(--color-secundary); padding: 20px 0px;">
    <div class="section-text wrapper">
        <h2 class="text-center" style="color:#fff;">Key Benefits</h2>
        <ul class="d-flex flex-wrap-mobile">
            <li style="background-color: var(--light); padding: 20px 15px; border-radius: 7px; box-shadow: 0px 0px 19px 5px rgba(0,0,0,0.29) inset;">
                <i class="fa fa-home" aria-hidden="true"></i>
                <h3>This is my title</h3>
                <p class="text-center font-catamaram">Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti molestiae pariatur commodi voluptatibus iusto quisquam adipisci saepe aliquam! Autem a</p>
            </li>
            <li style="background-color: var(--light); padding: 20px 15px; border-radius: 7px; box-shadow: 0px 0px 19px 5px rgba(0,0,0,0.29) inset;">
                <i class="fa fa-home" aria-hidden="true"></i>
                <h3>This is my title</h3>
                <p class="text-center font-catamaram">Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti molestiae pariatur commodi voluptatibus iusto quisquam adipisci saepe aliquam! Autem a</p>
            </li>
            <li style="background-color: var(--light); padding: 20px 15px; border-radius: 7px; box-shadow: 0px 0px 19px 5px rgba(0,0,0,0.29) inset;">
                <i class="fa fa-home" aria-hidden="true"></i>
                <h3>This is my title</h3>
                <p class="text-center font-catamaram">Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti molestiae pariatur commodi voluptatibus iusto quisquam adipisci saepe aliquam! Autem a</p>
            </li>
        </ul>
    </div>
</section>