<?
$h1         = 'Mapa do site';
$title      = 'Mapa do site';
$desc       = 'O mapa do site com todos os atalhos para todas as páginas deste site. Qualquer dúvida estamos a disposição por email ou telefone. Clicando aqui';
$key        = 'atalho para as páginas do site, mapa do site';
$var        = 'Mapa do site';

include('inc/head.php');
?>
<style>
  body {
    scroll-behavior: smooth;
  }
  <?
//   include ("$linkminisite"."css/mpi-product.css");
//   include ("$linkminisite"."css/mpi.css");
//   include ("$linkminisite"."css/aside.css");
//   include ("$linkminisite"."css/style-mpi.css");
include ("$linkminisite"."css/style.css");
  ?>
</style>
</head>

<body>

<? include ("$linkminisite"."inc/topo.php"); ?>

    <main>
        <div class="content">
            <section>
                <?= $caminho ?>
                <div class="wrapper">
                    <ul class="sitemap">

                        <li> <a href="<?= $url ?>">Inicio </a></li>


                        <li><a href="<?= $url ?>informacoes">Informações </a></li>

                        <? include('inc/sub-menu.php'); ?>

                        <li><a href="<?= $url ?>sobre-nos">Sobre Nós</a></li>
                        <li><a href="<?= $url ?>contato">Contato</a></li>
                    </ul>

                    <br class="clear">

            </section>
        </div>
    </main>
    </div><!-- .wrapper -->
    <? include('inc/footer.php'); ?>
    <script>
        $(document).ready(function() {
            if ($('.sitemap i').length > 0) {
                $('.sitemap i').each(function() {
                    $(this).remove();
                });
            }
        });
    </script>
</body>

</html>