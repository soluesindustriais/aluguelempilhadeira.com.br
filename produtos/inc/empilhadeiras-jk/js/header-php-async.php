<?php

$items = [
    ["text" => "Inicio", "link" => $linkminisite],
    ["text" => "Sobre nós", "url" => "sobre-nos", "link" => $url . "sobre-nos"],
    ["text" => "Contato", "url" => "contato", "link" => $url . "contato"],
    ["text" => "Informações", "url" => "informacoes", "dropdown" => true]
];


// Mapeia os títulos e links conforme os arrays anteriores
$menus = [];

// Adicionando o menu de Serviços apenas se houver itens
if (!empty($VetPalavrasInformacoes)) {
    $menus[] = [
        "title" => "Serviços",
        "items" => array_map(function ($item) use ($url) {
            $finalLink = (strpos($item, 'http') !== false ? $item : $url . $item);
            return [
                "title" => str_replace('-', ' ', ucwords($item)),
                "link" => $finalLink
            ];
        }, $VetPalavrasInformacoes)
    ];
}

// Adicionando o menu de Produtos apenas se houver itens
if (!empty($VetPalavrasProdutos)) {
    $menus[] = [
        "title" => "Produtos",
        "items" => array_map(function ($item) use ($url) {
            $finalLink = (strpos($item, 'http') !== false ? $item : $url . $item);
            return [
                "title" => str_replace('-', ' ', ucwords($item)),
                "link" => $finalLink
            ];
        }, $VetPalavrasProdutos)
    ];
};

