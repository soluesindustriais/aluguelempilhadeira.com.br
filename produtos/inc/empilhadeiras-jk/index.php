<?
$h1 = 'Início';
$title = 'Inicio';
$desc = 'Início';
$key = '';
$var = 'Início';
$linkminisite = "";
include('inc/head.php');

?>

<!-- SLICK CSS -->
<style>
	<?
	include('slick/slick.css');
	include('slick/slick-banner.css');
	include('css/home-3.css');
	include('css/header-script.css');
	include "$linkminisite" . "css/style.css";
	include "$linkminisite" . "css/mpi.css";
	include "$linkminisite" . "css/normalize.css";
	include "$linkminisite" . "css/aside.css";
	?>
</style>

</head>
<!-- END SLICK CSS -->

<body>

	<div class="content-gradient">


		<? include "inc/header-dinamic.php" ?>



		<div class="wrapper">
			<div class="content-main d-flex  align-items-center">
				<div class="content-home">
					

					<h1><?= $nomeSite ?></h1>

					<p> <?= $subTituloCliente ?></p>
					<div class="botao-cta d-flex justify-content-start flex-wrap">
						<a href="<?= $url ?>informacoes" title="Produtos">Produtos</a>
					</div>

				</div>
			</div>
		</div>
	</div>
	<!-- END SERVER-SIDE MOBILE DETECTION -->
	<main class="content-index">
		<?php
		include 'inc/section-content.php';
		/* include 'inc/feedback.php';
		include 'inc/blog-aside.php'; */
		// include 'inc/grid-mark.php';
		?>
	</main>
	<? include('inc/footer.php'); ?>
	<script>
		<? include('slick/slick.min.js'); ?>
		$(document).ready(function() {
			// SERVER-SIDE MOBILE DETECTION
			// END SERVER-SIDE MOBILE DETECTION
			// BANNER SAMPLE
			$('.slick-banner').slick({
				fade: true, // Remove this line to use Hero transition style
				cssEase: 'linear', // Remove this line to use Hero transition style
				autoplay: true,
				infinite: true,
				lazyLoad: 'ondemand',
				swipeToSlide: true
			});
			// SERVER-SIDE MOBILE DETECTION
			// END SERVER-SIDE MOBILE DETECTION
			// Other slick carousel calls goes here
		});
	</script>


</body>

</html>