<?php
$clienteAtivo = "ativo";

//HOMEsolucs
// $tituloCliente recebe ($clienteAtivo igual 'inativo') recebe 'Soluções industriais' se não 'Base Mini Site';
$tituloCliente = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'JK Empilhadeiras';
$tituloCliente = str_replace(' ', '-', $tituloCliente);

$subTituloCliente = 'Atuando no mercado de forma diferenciada';
$bannerIndex = 'imagem-banner-fixo';
$minisite = "inc/" . $tituloCliente . "/";
$subdominio = str_replace(' ', '-', $tituloCliente);

//informacoes-vetPalavras
$CategoriaNameInformacoes = "informacoes";


include("$linkminisite" . "conteudos-minisite.php");


// Criar página de produto mpi
$VetPalavrasProdutos = [
"acessorios-para-empilhadeiras",
"acessorios-para-empilhadeiras-eletricas",
"conserto-de-empilhadeira-eletrica",
"empresas-de-manutencao-de-empilhadeiras",
"manutencao-de-empilhadeiras-eletricas",
"manutencao-de-empilhadeiras-sp",
"pecas-para-empilhadeira-eletrica",
"reforma-de-empilhadeiras",
"roda-para-empilhadeira",
"rodas-para-empilhadeiras-eletricas"
];

//Criar página de Serviço
/* $VetPalavrasInformacoes = [

]; */



// Numero do formulario de cotação
$formCotar = 197;



// Informações Geral.php

$nomeSite = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'JK Empilhadeiras';
$slogan = ($clienteAtivo == 'inativo') ? "Inovando sua indústria com eficiência" : 'Atuando no mercado de forma diferenciada';
$rua = ($clienteAtivo == 'inativo') ? "Rua Alexandre Dumas" : 'Rua Doutor Rubens Noce, 200';
$bairro = ($clienteAtivo == 'inativo') ? "Santo Amaro" : 'Jardim Primavera';
$cidade = ($clienteAtivo == 'inativo') ? "São Paulo" : 'Várzea Paulista';
$UF = ($clienteAtivo == 'inativo') ? "SP" : 'SP';
$cep = ($clienteAtivo == 'inativo') ? "CEP: 04717-004" : '13220-300';
$imgLogo = ($clienteAtivo == 'inativo') ? 'logo-site.webp' : 'logo-cliente-2.webp';
$emailContato = ($clienteAtivo == 'inativo') ? '' : 'contato@empilhadeirasjk.com.br';
$instagram =  ($clienteAtivo == 'inativo') ? 'https://www.instagram.com/solucoesindustriaisoficial?igsh=MWtvOGF4aXBqODF3MQ==' : '';
$facebook =  ($clienteAtivo == 'inativo') ? 'https://www.facebook.com/solucoesindustriais/' : '';



?>


<!-- VARIAVEIS DE CORES -->
<style>
    :root {
        --cor-pretopantone: #2d2d2f;
        --cor-principaldocliente: #ed3237;
        --cor-secundariadocliente: #333;
        --terceira-cor: #2d2d2f;
        --font-primary: "Poppins";
        --font-secundary: "Poppins";
    }
</style>