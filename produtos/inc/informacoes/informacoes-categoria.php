<?php

  $categoriaNameSemAcento = strtolower(remove_acentos($CategoriaNameInformacoes)); // Remover acentos e substituir espaços por hifens

  foreach ($VetPalavrasInformacoes  as $palavra) {
      $palavraSemAcento = strtolower(remove_acentos($palavra)); // Remover acentos e substituir espaços por hifens
      $palavraUpperCaseSemHifen = ucwords(str_replace("-", " ", $palavra)); // Substituir hifens por espaços e colocar em maiúsculas

      // Gerar número aleatório entre 1 e 12
      $numeroAleatorio = rand(1, 12);

      // Construir a estrutura LI
      echo "<li>
              <div class=\"overflow-hidden\"> <a rel=\"nofollow\" href=\"" . $url . $palavraSemAcento . "\" title=\"$palavraUpperCaseSemHifen\"><img src=\"imagens/$categoriaNameSemAcento/$categoriaNameSemAcento-$numeroAleatorio.webp\" alt=\"$palavraUpperCaseSemHifen\" title=\"$palavraUpperCaseSemHifen\"></a> </div>
              <div class=\"title-produtos\">
                  <h2>$palavraUpperCaseSemHifen</h2>
              </div>
            </li>";
  }

  ?>