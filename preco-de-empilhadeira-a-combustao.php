<? $h1 = "Preço de empilhadeira a combustão";
$title  = "Preço de empilhadeira a combustão";
$desc = "Receba diversas cotações de $h1, descubra os melhores fornecedores, faça um orçamento hoje com 50 empresas ao mesmo tempo";
$key  = "Comprar de empilhadeira a combustão,Preço de empilhadeiras a combustão";
include('inc/head.php');
 ?></head>

<body><? include('inc/topo.php'); ?><div class="wrapper">
        <main>
            <div class="content">
                <section><?= $caminhoinformacoes ?><br class="clear" />
                    <h1><?= $h1 ?></h1>
                    <article>
                        <div class="img-mpi"><a href="<?= $url ?>imagens/mpi/preco-de-empilhadeira-a-combustao-01.jpg" title="<?= $h1 ?>" class="lightbox"><img src="<?= $url ?>imagens/thumbs/preco-de-empilhadeira-a-combustao-01.jpg" title="<?= $h1 ?>" alt="<?= $h1 ?>"></a><a href="<?= $url ?>imagens/mpi/preco-de-empilhadeira-a-combustao-02.jpg" title="Comprar de empilhadeira a combustão" class="lightbox"><img src="<?= $url ?>imagens/thumbs/preco-de-empilhadeira-a-combustao-02.jpg" title="Comprar de empilhadeira a combustão" alt="Comprar de empilhadeira a combustão"></a><a href="<?= $url ?>imagens/mpi/preco-de-empilhadeira-a-combustao-03.jpg" title="Preço de empilhadeiras a combustão" class="lightbox"><img src="<?= $url ?>imagens/thumbs/preco-de-empilhadeira-a-combustao-03.jpg" title="Preço de empilhadeiras a combustão" alt="Preço de empilhadeiras a combustão"></a></div><span class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível livremente na internet</span>
                        <hr />
                        <p>O <strong>Preço de empilhadeira a combustão</strong> é um equipamento automático, conta com transmissão Powershift e direção hidrostática. Seu motor Toyota GLP garante segurança no manuseio e qualidade no serviço executado.</p>
                        <h2>Confira as características do <strong>Preço de empilhadeira a combustão</strong></h2>
                        <ul>
                            <li class="li-mpi">Capacidade de Carga 2.500 Kg</li>
                            <li class="li-mpi">Centro de Carga 500 mm</li>
                            <li class="li-mpi">Mastro Telescópico Triplex</li>
                            <li class="li-mpi">Altura de elevação dos garfos 4.800 mm</li>
                            <li class="li-mpi">Altura com o mastro abaixado 2.145 mm</li>
                            <li class="li-mpi">Comprimento dos garfos 1.070 mm</li>
                        </ul>
                        <p>O <strong>Preço de empilhadeira a combustão</strong> conta ainda, com ignição eletrônica, cinto de segurança retrátil, proteção do operador, coluna de direção com ajuste de inclinação, pneus superelásticos, cilindro de inclinação e deslocador lateral dos garfos. Para maior conforto do operador, possui cabine com sistema FCS (Full Suspended Cab).</p>
                        <p>As empresas especializadas e com tradição no comércio e locação de empilhadeiras a combustão e elétricas. Para obter mais informações ou dúvidas, entre em contato com a empresa responsável e conheça equipamentos de qualidade.</p>
                        <p>Solicite agora mesmo orçamento para <strong>Preço de empilhadeira a combustão</strong>!</p>
                    </article><? include('inc/coluna-mpi.php'); ?><br class="clear"><? include('inc/busca-mpi.php'); ?><? include('inc/form-mpi.php'); ?><? include('inc/regioes.php'); ?>
                </section>
            </div>
        </main>
    </div><? include('inc/footer.php'); ?></body>

</html>