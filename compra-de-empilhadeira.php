<? $h1 = "Compra de empilhadeira";
$title  = "Compra de empilhadeira";
$desc = "Faça um orçamento de $h1, ache os melhores distribuidores, cote produtos hoje com aproximadamente 200 fornecedores de todo o Brasil";
$key  = "Compras de empilhadeira,Compra de empilhadeiras";
include('inc/head.php');  ?></head>

<body><? include('inc/topo.php'); ?><div class="wrapper">
        <main>
            <div class="content">
                <section><?= $caminhoinformacoes ?><br class="clear" />
                    <h1><?= $h1 ?></h1>
                    <article>
                        <div class="img-mpi"><a href="<?= $url ?>imagens/mpi/compra-de-empilhadeira-01.jpg" title="<?= $h1 ?>" class="lightbox"><img src="<?= $url ?>imagens/thumbs/compra-de-empilhadeira-01.jpg" title="<?= $h1 ?>" alt="<?= $h1 ?>"></a><a href="<?= $url ?>imagens/mpi/compra-de-empilhadeira-02.jpg" title="Compras de empilhadeira" class="lightbox"><img src="<?= $url ?>imagens/thumbs/compra-de-empilhadeira-02.jpg" title="Compras de empilhadeira" alt="Compras de empilhadeira"></a><a href="<?= $url ?>imagens/mpi/compra-de-empilhadeira-03.jpg" title="Compra de empilhadeiras" class="lightbox"><img src="<?= $url ?>imagens/thumbs/compra-de-empilhadeira-03.jpg" title="Compra de empilhadeiras" alt="Compra de empilhadeiras"></a></div><span class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível livremente na internet</span>
                        <hr />
                        <p>A<strong> compra de empilhadeira</strong>tem garantia e procedência da empresa Lotvs, elas provêm da frota de locação da empresa, de consumidores e parceiros.</p>
                        <h2>Modelos</h2>
                        <ul>
                            <li class="li-mpi">Empilhadeira Ameise ETV 849;</li>
                            <li class="li-mpi">Empilhadeira Especial para Pneus;</li>
                            <li class="li-mpi">Empilhadeira Patolada Feeler;</li>
                            <li class="li-mpi">Empilhadeira Retrátil Still FME 17;</li>
                            <li class="li-mpi">Empilhadeira Retrátil Feeler 2.0 TON – 2012;</li>
                            <li class="li-mpi">Empilhadeira Tri-lateral Raymond;</li>
                            <li class="li-mpi">Empilhadeira Retrátil – Hyster Matrix 1.6H.</li>
                        </ul>
                        <p>Novos ou seminovos, os equipamentos tem garantia de excelente custo-benefício e rentabilidade. Fáceis de conduzir, econômicos e com alta durabilidade, as empilhadeiras também garantem a segurança dos colaboradores, pois possuem a mais alta tecnologia do mercado. São duas de suas variáveis a capacidade de carga e a elevação máxima.</p>
                        <h2>Informações adicionais</h2>
                        <p>O portfólio vai além de <strong>compra de empilhadeira</strong>, pois também conta com acessórios como tomadas, bombas hidráulicas, coroa e pinhão, cilindros de freios para empilhadeiras, lona de freio, entre outros.</p>
                    </article><? include('inc/coluna-mpi.php'); ?><br class="clear"><? include('inc/busca-mpi.php'); ?><? include('inc/form-mpi.php'); ?><? include('inc/regioes.php'); ?>
                </section>
            </div>
        </main>
    </div><? include('inc/footer.php'); ?></body>

</html>