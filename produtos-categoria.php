<?
	$h1 = "Produtos";
	$title = "Produtos";
	$desc = "Informações sobre os produtos e serviços comercializados pela empresa. Clique aqui para saber mais detalhes. Dúvidas, entre em contato conosco";
	$var = "Produtos";
	include('inc/head.php');
?>
	</head>
	<body>
	<? include('inc/topo.php');?>
	<div class="wrapper">
	 	<main>
	 		<div class="content">
	 			<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/breadcrumb">
	 				<a rel="home" itemprop="url" href="http://www.empilhadeiranova.com.br" title="home">
	 					<span itemprop="title">
	 						<i class="fa fa-home" aria-hidden="true"></i>Home
	 					</span>
	 				</a> »
					<strong><span class="page" itemprop="title">Produtos</span></strong>
	 			</div>

				<h1>Produtos</h1>
				
				<article class="full">
					<p>Encontre diversos fornecedores de empilhadeiras, cote agora mesmo!</p>

					<ul class="thumbnails-main">

						<li>
							<a  href="<?=$url?>aluguel-de-empilhadeira-e-acessorios-para-docas-categoria" title="Categoria - Aluguel de empilhadeira e acessorios para docas">
								<img src="<?$url?>imagens/aluguel-de-empilhadeira-e-acessorios-para-docas-1.jpg" alt="Aluguel de empilhadeira e acessorios para docas" title="Aluguel de empilhadeira e acessorios para docas"/>
							</a>
							<h2>
								<a href="<?=$url?>aluguel-de-empilhadeira-e-acessorios-para-docas-categoria" title="Categoria -aluguel de empilhadeira e acessorios para docas">
									Aluguel de empilhadeira e acessorios para docas
								</a>
							</h2>
						</li>

						<li>
							<a  href="<?=$url?>empilhadeiras-categoria" title="Categoria - Empilhadeiras">
								<img src="<?$url?>imagens/empilhadeiras/empilhadeiras-6.jpg" alt="EmpilhadeiraS" title="Empilhadeiras"/>
							</a>
							<h2>
								<a href="<?=$url?>empilhadeiras-categoria" title="Categoria - Empilhadeiras">
									Empilhadeiras
								</a>
							</h2>
						</li>

						<li>
							<a  href="<?=$url?>carrinho-rebocador-categoria" title="Categoria - Carrinho Rebocador">
								<img src="<?$url?>imagens/carrinho-rebocador/carrinho-rebocador-1.jpg" alt="Carrinho Rebocador" title="Carrinho Rebocador"/>
							</a>
							<h2>
								<a href="<?=$url?>carrinho-rebocador-categoria" title="Categoria - Carrinho Rebocador">
									Empilhadeiras
								</a>
							</h2>
						</li>

						

					</ul>
				</article>

	 		</div>
	 	</main>
	 </div>
	 <? include('inc/footer.php');?>
	</body>
</html>