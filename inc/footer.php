<footer>
	<div class="wrapper">
		<div class="contact-footer">
			<address>
				<span><?= $nomeSite . " - " . $slogan ?></span>
			</address>
		</div>
		<div class="menu-footer">
			<nav>
				<ul>
					<li><a href="" title="Página inicial">Home</a></li>
					<li><a href="aluguel-de-empilhadeira-e-acessorios-para-docas-categoria" title="Produtos">Produtos</a></li>
					<li><a href="informacoes" title="Informacoes">Informações</a></li>
					<li><a href="sobre-nos" title="Sobre nós">Sobre nós</a></li>
					<li><a href="mapa-site" title="Mapa do site">Mapa do site</a></li>
				</ul>
			</nav>
		</div>
		<br class="clear">
	</div>
</footer>
<div class="copyright-footer">

	<div class="center-footer">
		<p>Copyright © <?= $nomeSite ?>. (Lei 9610 de 19/02/1998)</p>
		<img src="imagens/img-home/logo-footer.png" alt="<?= $nomeSite ?>" title="<?= $nomeSite ?>">
		<p>é um parceiro</p>
		<img src="imagens/logo-solucs.png" alt="Soluções Industriais" title="Soluções Industriais">
		<div class="selos">
			<a rel="noopener nofollow" href="https://validator.w3.org/check?uri=<?= $url . $urlPagina ?>" target="_blank" title="HTML5 W3C"><i class="fab fa-html5"></i> <strong>W3C</strong></a>
			<a rel="noopener nofollow" href="https://jigsaw.w3.org/css-validator/validator?uri=<?= $url . $urlPagina ?>" target="_blank" title="CSS W3C"><i class="fab fa-css3"></i> <strong>W3C</strong></a>
		</div>
	</div>
</div>
</div>
<div class="wrapper-footer">


<? include('inc/fancy.php'); ?>
	<script defer src="<?= $url ?>js/geral.js"></script>
	<!-- Google Analytics -->
	<script>
		(function(i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function() {
					(
						i[r].q = i[r].q || []).push(arguments)
				},
				i[r].l = 1 * new Date();
			a = s.createElement(o),
				m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
		ga('create', 'UA-122112734-1', 'auto');
		ga('create', 'UA-47730935-51', 'auto', 'clientTracker');
		ga('send', 'pageview');
		ga('clientTracker.send', 'pageview');
	</script>
	<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-NLJR0TKE7X"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-NLJR0TKE7X');
</script>
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-D23WW3S4NC"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-D23WW3S4NC');
</script>
	<!-- Shark Orcamento -->
<div id="sharkOrcamento" style="display: none;"></div>
<script>
	var guardar = document.querySelectorAll('.botao-cotar');
		for(var i = 0; i < guardar.length; i++){
		guardar[i].removeAttribute('href');
  var adicionando = guardar[i].parentNode;
	adicionando.classList.add('nova-api');
};
</script>
<script src="//code-sa1.jivosite.com/widget/SjER3i00S8" async></script>
<script src="https://solucoesindustriais.com.br/js/dist/sdk-cotacao-solucs/package.js"></script>


	<!-- Script Launch start -->
	<script src="https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js" defer></script>
	<script>
		const aside = document.querySelector('aside');
		const data = '<div data-sdk-ideallaunch data-segment="Soluções Industriais - Oficial"></div>';
		aside != null ? aside.insertAdjacentHTML('afterbegin', data) : console.log("Não há aside presente para o Launch");
	</script> <!-- Script Launch end -->

<?php include 'inc/fancy.php'; ?><div style="display: none" id="exit-banner-div">
    <div data-sdk-ideallaunch="" data-placement="popup_exit" id="exit-banner-container"></div>
</div>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.5/dist/js.cookie.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />

<script src='https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js'></script>

<script>
    setTimeout(function() {
        $("html").mouseleave(function() {
            if ($("#exit-banner-container").find('img').length > 0 &&
                !Cookies.get('banner_displayed')) {
                if ($('.fancybox-container').length == 0) {
                    $.fancybox.open({
                        src: '#exit-banner-div',
                        type: 'inline',
                        opts: {
                            afterShow: function() {
                                let minutesBanner = new Date(new Date().getTime() + 5 * 60 * 1000);
                                Cookies.set('banner_displayed', true, { expires: minutesBanner });
                            }
                        }
                    });
                }
            }
        });
    }, 4000);
</script>