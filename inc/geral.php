<?
$nomeSite			= 'Aluguel Empilhadeira';
$slogan				= 'Conte com os melhores fornecedores de Empilhadeira do Brasil';
//$url				= 'https://www.aluguelempilhadeira.com.br/';
//$url				= 'http://localhost/growth/bkps/bkps-solucs/aluguelempilhadeira.com.br/';

$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") { $url = $http."://".$host."/"; }
else { $url = $http."://".$host.$dir["dirname"]."/";  }
	
$emailContato		= 'everton.lima@solucoesindustriais.com.br';
$rua				= 'Rua Pequetita, 179';
$bairro				= 'Vila Olimpia';
$cidade				= 'São Paulo';
$UF					= 'SP';
$cep				= 'CEP: 04552-060';
$latitude			= '-22.546052';
$longitude			= '-48.635514';
$idAnalytics		= 'UA-122112734-1';
$senhaEmailEnvia	= '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode			= explode("/", $_SERVER['PHP_SELF']);
$urlPagina 			= end($explode);
$urlPagina	 		= str_replace('.php','',$urlPagina);
$urlPagina 			== "index"? $urlPagina= "" : "";
$urlAnalytics = str_replace("https://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
//reCaptcha do Google
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';
//Breadcrumbs
$caminho 			= '
<div id="breadcrumb" itemscope itemtype="https://data-vocabulary.org/Breadcrumb" >
	<a rel="home" itemprop="url" href="'.$url.'" title="Home"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
</div>
';
$caminho2	= '
<div id="breadcrumb" itemscope itemtype="https://data-vocabulary.org/Breadcrumb" >
	<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
		<a href="'.$url.'produtos-categoria" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
		</div>
	</div>
</div>
';
$caminhoBread 			= '
<div class="wrapper">
	<div id="breadcrumb" itemscope itemtype="https://data-vocabulary.org/Breadcrumb" >
		<a rel="home" itemprop="url" href="'.$url.'" title="Home"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
		<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
	</div>
	<h1>'.$h1.'</h1>
</div>
</div>
';
$caminhoinformacoes	= '
<div class="wrapper">
<div id="breadcrumb" itemscope itemtype="https://data-vocabulary.org/Breadcrumb" >
	<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
		<a href="'.$url.'informacoes" title="Informações" class="category" itemprop="url"><span itemprop="title"> Informações </span></a> »
		<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
		</div>
	</div>
</div>
</div>

';
$caminhocalibracao	= '
<div class="wrapper">
<div id="breadcrumb" itemscope itemtype="https://data-vocabulary.org/Breadcrumb" >
	<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
		<a href="'.$url.'produtos-categoria" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
			<a href="'.$url.'calibracao-categoria" title="Calibração" class="category" itemprop="url"><span itemprop="title"> Calibração </span></a> »
			<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
				<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
			</div>
		</div>
	</div>
</div>
</div>
';
$caminhocalibracao_de_balancas	= '
<div class="wrapper">
<div id="breadcrumb" itemscope itemtype="https://data-vocabulary.org/Breadcrumb" >
	<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
		<a href="'.$url.'produtos-categoria" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
			<a href="'.$url.'calibracao-de-balancas-categoria" title="Calibração de Balanças" class="category" itemprop="url"><span itemprop="title"> Calibração de Balanças </span></a> »
			<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
				<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
			</div>
		</div>
	</div>
</div>
</div>
';

$caminhoempilhadeiras	= '
<div class="wrapper">
<div id="breadcrumb" itemscope itemtype="https://data-vocabulary.org/Breadcrumb" >
	<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
		<a href="'.$url.'produtos-categoria" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
			<a href="'.$url.'empilhadeiras-categoria" title="Empilhadeiras" class="category" itemprop="url"><span itemprop="title"> Empilhadeiras </span></a> »
			<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
				<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
			</div>
		</div>
	</div>
</div>
</div>
';

$caminhoaluguel_de_empilhadeira_e_acessorios_para_docas	= '
<div class="wrapper">
<div id="breadcrumb" itemscope itemtype="https://data-vocabulary.org/Breadcrumb" >
	<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
		<a href="'.$url.'produtos-categoria" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
			<a href="'.$url.'aluguel-de-empilhadeira-e-acessorios-para-docas-categoria" title="Aluguel de empilhadeira e acessorios para docas" class="category" itemprop="url"><span itemprop="title"> Aluguel de empilhadeira e acessorios para docas </span></a> »
			<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
				<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
			</div>
		</div>
	</div>
</div>
</div>
';

$caminhocarrinho_rebocador	= '
<div class="wrapper">
<div id="breadcrumb" itemscope itemtype="https://data-vocabulary.org/Breadcrumb" >
	<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
		<a href="'.$url.'produtos-categoria" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
			<a href="'.$url.'carrinho-rebocador-categoria" title="Carrinho Rebocador" class="category" itemprop="url"><span itemprop="title"> Carrinho Rebocador </span></a> »
			<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
				<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
			</div>
		</div>
	</div>
</div>
</div>
';

?>