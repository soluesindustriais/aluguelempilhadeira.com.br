<div id="author" class="blog-grid">
	<section>
		<?= $caminho ?>

		<div class="bg-light author__heading">
			<div class="container">
				<div class="wrapper">
					<div class="row gap-50 align-items-center">
						<div class="col-3">
							<div class="author__cover">
								<img class="author__image" src="<?= RAIZ ?>/doutor/uploads/<?= $authorCover ?>" alt="<?= $authorName ?>" title="<?= $authorName ?>">
							</div>
						</div>

						<div class="col-9">
							<div class="author__info">
								<h2 class="author__title"><?= $authorName . " " . $authorLastName ?></h2>
								<h3 class="author__role"><?= $authorRole ?></h3>
								<p class="author__text"><?= $authorAbout ? $authorAbout : "N/D" ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="wrapper">
				<div class="article-container">
					<?php

					$Read->ExeRead(TB_BLOG, "WHERE user_id = :usr AND blog_status = :stats ORDER BY blog_date DESC LIMIT :limit OFFSET :offset", "usr=" . $itemAuthor . "&stats=2&limit={$Pager->getLimit()}&offset={$Pager->getOffset()}");
					if (!$Read->getResult()) :
						WSErro("Nenhuma postagem encontrada para o autor <b>{$s}</b>.", WS_INFOR, null, "Resultado da pesquisa");
					else : ?>
						<div class="grid-col-2">
							<?php
							foreach ($Read->getResult() as $blog) :
								extract($blog); ?>
								<div class="blog-card">
									<div class="blog-card__image">
										<a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $blog_name; ?>" title="<?= $blog_title; ?>">
											<img class="blog-card__cover" src="<?= RAIZ ?>/doutor/uploads/<?= $blog_cover ?>" alt="<?= $blog_title ?>" title="<?= $blog_title ?>">
										</a>
										<?php
											$blogCatUrl = Check::CatByParent($cat_parent, EMPRESA_CLIENTE);
											$blogCatUrlFinal = explode("/", $blogCatUrl); 
											array_pop($blogCatUrlFinal);                       
											$blogCatTitle = Check::CatByUrl(end($blogCatUrlFinal), EMPRESA_CLIENTE);
											$blogCatUrl = RAIZ . "/". substr($blogCatUrl, 0, -1);
										?>
										<a class="blog-card__category" href="<?=$blogCatUrl?>" title="<?=$blogCatTitle?>"><?=$blogCatTitle?></a>
									</div>
									<div class="blog-card__info">
										<h3 class="blog-card__title"><a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $blog_name; ?>" title="<?= $blog_title; ?>">
												<?= $blog_title ?>
											</a></h3>
										<?php $newDate = explode("/", date("d/m/Y", strtotime($blog_date)));
										$blogDay = $newDate[0];
										$blogMonth = $newDate[1];
										$blogYear = $newDate[2];
										$blogFullDate = $blogDay . " de " . $blogMonthList[$blogMonth - 1] . " de " . $blogYear;
										?>
										<p class="blog-card__date"><?= $blogFullDate ?></p>
										<div class="blog-card__author">
											Por:
											<a href="<?= $url ?>autor/<?= urlencode($authorName) ?>" rel="nofollow" title="<?= $authorName ?>"><?= $authorName ?></a>
										</div>

										<div class="blog-card__description">
											<?php if (BLOG_BREVEDESC && isset($blog_brevedescription)) : ?>
												<p class="blog-card__content-text"><?= $blog_brevedescription ?></p>
											<?php else : ?>
												<p class="blog-card__content-text"><?= Check::Words($blog_content, 25); ?></p>
											<?php endif; ?>
										</div>
									</div>
								</div>
							<? endforeach; ?>
						</div>

						<div class="blog-pagination d-flex justify-content-center">
							<?php
							$Pager->ExePaginator(TB_BLOG, "WHERE user_id = :usr AND blog_status = :stats ORDER BY blog_date DESC", "usr=" . $itemAuthor . "&stats=2");
							echo $Pager->getPaginator();
							?>
						</div>
					<? endif; ?>
				</div>

				<? include('doutor/layout/blog-grid/aside-sig-grid.php'); ?>
			</div>
		</div>
	</section>
</div>