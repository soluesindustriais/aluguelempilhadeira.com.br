<!-- page content -->
<div class="right_col" role="main">  
    <div class="row">
        <?php
        $lv = 2;
        if (!APP_USERS || empty($userlogin) || $user_level < $lv):
            die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
        endif;
        ?>	
    </div>
    <form data-parsley-validate class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
        <div class="page-title">
            <div class="title col-md-12 col-sm-6 col-xs-12">
                <h3><i class="fa fa-download"></i>  Atualização de download</h3>
            </div>
            <div class="clearfix"></div>
            <br/>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="pull-right">
                        <button type="submit" name="Update" class="btn btn-primary"><i class="fa fa-save"></i></button>
                        <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=downloads/index'"><i class="fa fa-home"></i></button>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <?php
                    $get = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
                    $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
                    if (isset($post) && isset($post['Update'])):
                        unset($post['Update']);
                        $post['user_empresa'] = $_SESSION['userlogin']['user_empresa'];
                        $post['dow_file'] = ( $_FILES['dow_file']['tmp_name'] ? $_FILES['dow_file'] : null );
                        $post['dow_id'] = $get;

                        $update = new Downloads();
                        $update->ExeUpdate($post);

                        if (!$update->getResult()):
                            $erro = $update->getError();
                            WSErro($erro[0], $erro[1], null, $erro[2]);
                        else:
                            $_SESSION['Error'] = $update->getError();
                            header('Location: painel.php?exe=downloads/index&get=true');
                        endif;
                    endif;

                    $ReadRecursos = new Read;
                    $ReadRecursos->ExeRead(TB_DOWNLOAD, "WHERE user_empresa = :emp AND dow_id = :id", "emp={$_SESSION['userlogin']['user_empresa']}&id={$get}");
                    if ($ReadRecursos->getResult()):
                        foreach ($ReadRecursos->getResult() as $dados):
                            extract($dados);
                            ?>                   
                            <div class="x_panel">
                                <div class="x_content">
                                    <div class="x_title">
                                        <h2>Edite o download.</h2>                           
                                        <div class="clearfix"></div>                            
                                    </div>

                                    <div class="clearfix"></div>
                                    <br>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_genero">Publicar agora?</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <div class="radio">
                                                <label>
                                                    <input name="dow_status" id="optionsRadios1" required="required" type="radio" value="2" <?php
                                                    if ($post['dow_status'] == 2): echo 'checked="true"';
                                                    elseif ($dow_status == 2): echo 'checked="true"';
                                                    endif;
                                                    ?>> Sim
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input name="dow_status" id="optionsRadios2" type="radio" value="1" <?php
                                                    if ($post['dow_status'] == 1): echo 'checked="true"';
                                                    elseif ($dow_status == 1): echo 'checked="true"';
                                                    endif;
                                                    ?>> Não
                                                </label>
                                            </div>                 
                                        </div>                                 
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cat_parent">Sessão ou categoria do item <span class="required">*</span></label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <!--inclusão das seleções de categorias-->
                                            <?php include("inc/categorias.inc.php"); ?>
                                            <!-- /inclusão das seleções de categorias-->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dow_file">Arquivo <span class="text-info">(.pdf, .doc, .xlsx, .csv)</span> <span class="text-danger">ao reenviar, o arquivo antigo será substituído</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">                                            
                                            <div class="clearfix"></div>
                                            <br/>
                                            <input type="file" id="app_file" name="dow_file">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dow_title">Nome <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" id="dow_title" name="dow_title" required="required" class="form-control col-md-7 col-xs-12" value="<?php
                                            if (isset($post['dow_title'])): echo $post['dow_title'];
                                            else: echo $dow_title;
                                            endif;
                                            ?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dow_description">Breve descrição <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <textarea name="dow_description" required="required" class="form-control col-md-7 col-xs-12"><?php
                                                if (isset($post['dow_description'])): echo $post['dow_description'];
                                                else: echo $dow_description;
                                                endif;
                                                ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </div>
            </div>
        </div>
        <div class="pull-right">
            <button type="submit" name="Update" class="btn btn-primary"><i class="fa fa-save"></i></button>
            <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=downloads/index'"><i class="fa fa-home"></i></button>
        </div>
        <div class="clearfix"></div>
        <br>
    </form>
    <div class="clearfix"></div>
</div>
