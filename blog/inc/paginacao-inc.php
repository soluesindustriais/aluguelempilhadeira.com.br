<?php
$PerPage = filter_input(INPUT_GET, 'perpage', FILTER_VALIDATE_INT);
$PerPage = (!empty($PerPage) || isset($PerPage) ? $PerPage : MIN_PER_PAGE);
$Page = filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT);
$Pager = new Pager(RAIZ . Check::AmmountURL($URL) . "&perpage={$PerPage}&page=");
$Pager->ExePager($Page, $PerPage);
?>
<div class="container">
  <form class="form__pages" method="post">
    <div class="search__pages">
      <input type="search" name="busca" placeholder="Pesquisar..."  required/>
      <button type="submit" name="pesquisar" value="Pesquisar"><i class="fa fa-search"></i></button>
    </div>
    <div class="view__pages">
      <label>Exibir</label>
      
      <select name="PerPage" class="j_change" data-url="<?= RAIZ . Check::AmmountURL($URL); ?>">
        <?php for ($mult = 1; $mult <= MULTIPLO_PAGE; $mult++): ?>
          <option value="<?= $mult * MIN_PER_PAGE; ?>" <?php
          if (isset($PerPage) && $PerPage == ($mult * MIN_PER_PAGE)): echo 'selected="selected"';
          endif;?>><?= $mult * MIN_PER_PAGE; ?></option>
        <?php endfor; ?>
      </select>
      
      <label>Por página</label>
    </div>
  </form>
</div>
<div class="clear"></div>